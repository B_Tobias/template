temp.autoSave = function () {
	var currently = 0,
	checkStorage = function (callBack) {
		if (temp.autoSave.support)
			callBack();
		else
			alert('Sorry! No Web Storage support..');

	},
	getActualDate = function () {
		var today = new Date();
		return today.getFullYear() + '.' + String(Number(today.getMonth()) + 1) + '.' + today.getDate() + //2017.05.12
		' ' + today.getHours() + ':' + today.getMinutes(); //18:59
	};

	return {
		support: false,
		saveHandler: function (currently) {
			$('#ifWorkTable').off('click');
			$('#ifWorkTable').click(function () {
				localStorage.setItem(currently, JSON.stringify({
						date: getActualDate(),
						html: $('#ghostDom').contents().find('html').html(),
						name: $('#projectName').val(),
						num: currently
					}));
				console.log('asd');
			});
		},
		createStorageData: function () {
			//increase maxNum
			currently = localStorage.getItem("maxNum");
			currently++;
			localStorage.setItem("maxNum", currently);
			//set autosave
			temp.autoSave.saveHandler(currently);
		},
		loadHandler: function () {
			var recentDataArr = function () {
				var item = '',
				res = [];
				for (var i = 1; i <= localStorage.getItem("maxNum"); i++) {
					item = JSON.parse(localStorage.getItem(i));
					if (typeof item !== 'null')
						res.push(item);
				}
				return res;
			}
			(),
			setClickOnArticle = function (currentlyObj) {
				return function () {
					//storage
					currently = currentlyObj.num;
					//set autosave
					temp.autoSave.saveHandler(currently);
					//switch to worktable
					temp.ui.ifRecent.toWorkTable(currentlyObj.html);
				};
			};
			$('#ifRecent>article').remove();
			//sort recentDataArr by date
			/*res.sort(function (a, b) {
			return a.date - b.date;
			});*/
			//append articles with click event
			for (var key in recentDataArr) {
				if (key) {
					currentlyObj = recentDataArr[key];
					//create element
					$(temp.html.article.replace('NAME', currentlyObj['name'] + ' - ' + currentlyObj['date']).replace('DESCRIPTION', currentlyObj['html'].slice(0, 20)))
					.appendTo('#ifRecent')
					.click(setClickOnArticle(currentlyObj));
				}
			}
		},
		init: function () {
			temp.autoSave.saveHandler();
		}
	}
}
();
