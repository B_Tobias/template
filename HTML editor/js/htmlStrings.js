temp.html = {
	article: `
		<article class="template-article">
			<h1>NAME</h1>
			<p>DESCRIPTION</p>
		</article>`, 
	defaultTemplate: `
		<!doctype html>
		<html lang="">
			<head>
				<meta charset="utf-8">
				<title></title>
				<meta name="description" content="">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<link rel="stylesheet" href="css/holyStyle.css">
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" class="remove-me"></script>
			</head>
			<body>
				<script>window.ui=window.ui||{};</script> 
			</body>
		</html>`, 
	testTemplate: `
		<!doctype html>
		<html lang="">
			<head>
				<meta charset="utf-8">
				<title></title>
				<meta name="description" content="">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<link rel="stylesheet" href="css/holyStyle.css">
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" class="remove-me"></script>
			</head>
			<body>
				<div class="first part part-container child-container">
					<div class="second part part-container child-container">
						<div class="fourth part part-container child-container">
						</div>
						<div class="fifth part part-container child-container">
						</div>
					</div>
					<div class="third part part-container child-container">
					</div>
				</div>
				<script>
					window.ui=window.ui||{};
				</script>
			</body>
		</html>

	`, 
	partDetail: `
		<details class="detail">
			<summary>
				<input type="checkbox" class="select-detail">
				<b class="part-type">PARTTYPE</b>
				<span class="show-content">Content</span>
				<span class="show-children">Children</span>
				<span class="show-set-part">SetPart</span>
			</summary>
			<section class="detail-content"></section>
			<section class="detail-children"></section>
			<section class="detail-set-part">
				<select class="detail-set-container"></select>
			</section>
		</details>`, 
	propertyString: `
		<div class='detail-property detail-property-string'>
			<span>PROPNAME</span>
			<input type="text" class='detail-prop-value prop-name-PROPNAME'></input>
		</div>`, 
	propertyOptional: `
		<div class='detail-property detail-property-optional'>
			<span>PROPNAME</span>
			<select class='detail-prop-value prop-name-PROPNAME'>OPTION</select>
		</div>`, 
	detailSetType: 
			`<section class="detail-type-first-time">
				<select class="detail-set-type-first-time"></select>			
				<button class="detail-set-type-first-time-button">Set type</button>
			</section>`,
	detailSetContainer: 
			`<section class="detail-container-first-time">
				<select class="detail-set-container-first-time"></select>	
				<button class="detail-set-container-first-time-button">Set container</button>
			</section>`
};
