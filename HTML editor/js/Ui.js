temp.ui = function () {
	var $activeIf, //actually displayed interface
	templateClass = function (name, description, html) {
		return {
			'name': name,
			'description': description,
			'html': html
		}
	},
	toWorkTable = function (htmlCode) {
		//clear domTree:
		$('#domTree').empty();
		temp.treatDom.loadGhostDom(htmlCode);
		temp.treatDom.loadToWorkTable();
		temp.ui.interfaceChanger(temp.ui.ifWorkTable.$section);
	};
	return {
		interfaceChanger: function ($ifTo) {
			//hide older
			if ($activeIf)
				$activeIf.css('display', 'none');

			//trigger for if change
			$ifTo.trigger('itBecomeActive', $activeIf); //$activeIf will be the older if
			//show new
			$ifTo.css('display', 'block');
			//save active
			$activeIf = $ifTo;

		},
		ifHome: function () {
			return {
				$section: $("#ifHome"),
				createHandler: function () {
					temp.ui.interfaceChanger(temp.ui.ifCreate.$section);
				},
				openHandler: function () {
					console.log('upload and open in worktable');
					//****
					$("#uploadedProject").change(function () {
						var reader = new FileReader(),
						file = this.files[0];

						reader.onload = function (progressEvent) {
							toWorkTable(this.result);
						};
						reader.readAsText(file);
					});
					$("#uploadedProject").click();
					//***
				},
				openFromRecentHandler: function () {
					temp.ui.interfaceChanger(temp.ui.ifRecent.$section);
				},
				ifEventFunc: function () {
					$("#ifHomeCreateButton").click(this.createHandler);
					$("#ifHomeOpenButton").click(this.openHandler);
					$("#ifHomeOpenFromRecentButton").click(this.openFromRecentHandler);
				},
				init: function () {
					this.ifEventFunc();
				}
			}
		}
		(),
		ifCreate: function () {
			return {
				$section: $("#ifCreate"),
				//template obj-arr
				templateHtml: {
					'default': templateClass('Default', 'This is an empty, very simple template.', temp.html.defaultTemplate),
					'test': templateClass('Test', 'Container in container - test template', temp.html.testTemplate),
				},
				createTemplate: function (obj) {
					return function () {
						toWorkTable(obj.html);
						temp.autoSave.createStorageData();
					};
				},
				ifEventFunc: function () {
					var templateArr = this.templateHtml,
					articleHtml = temp.html.article,
					$ifCreate = $('#ifCreate'),
					actualObj;

					//load templates from template arr-obj and set events
					for (var key in templateArr) {
						if (key) {
							actualObj = templateArr[key];
							//create element
							$(articleHtml.replace('NAME', actualObj['name']).replace('DESCRIPTION', actualObj['description'])).appendTo($ifCreate).click(temp.ui.ifCreate.createTemplate(actualObj));
						}

					}
				},
				init: function () {
					this.ifEventFunc();
				}
			}
		}
		(),
		ifRecent: function () {
			return {
				$section: $("#ifRecent"),
				getRecentFromLocalStorage: function () {
					temp.autoSave.loadHandler();
				},
				toWorkTable: function (html) {
					toWorkTable(html);
				},
				onActive: function (old) {
					temp.ui.ifRecent.getRecentFromLocalStorage();
				},
				ifEventFunc: function () {
					this.$section.on('itBecomeActive', temp.ui.ifRecent.onActive);
					//back to menu icon event
					temp.ui.ifRecent.$section.find($(".back-to-menu")).click(function () {
						temp.ui.interfaceChanger(temp.ui.ifHome.$section);
					});
				},
				init: function () {
					this.ifEventFunc();
				}
			}
		}
		(),
		ifWorkTable: function () {
			return {
				$section: $("#ifWorkTable"),
				downloadProject: function () {
					temp.ui.interfaceChanger(temp.ui.ifDownloadProject.$section);
				},
				homeButton: function () {
					temp.ui.interfaceChanger(temp.ui.ifHome.$section);
				},
				setDetailIf: function ($det, interF) {
					//interF poteintial values:content,children,set-part,type-first-time,container-first-time
					if ($det.children('.detail-' + interF).css('display') != 'block') {
						$det.children('section').css('display', 'none');
						$det.children('.detail-' + interF).css('display', 'block');
					}
				},
				detailIfChanger: function ($det) {
					var typeSetter = {},
					spans = $det.children('summary').find('span'),
					changeIf = function (nthSpan) {
						$(spans[nthSpan]).click(function (e) {
							e.preventDefault();
							temp.ui.ifWorkTable.setDetailIf($det, $(spans[nthSpan]).attr('class').replace('show-', ''));
						});
					};
					//if changer for detail
					spans.map(changeIf);
				},
				ifEventFunc: function () {
					//Back to home
					$("#homePage").click(this.homeButton);
					//change to donwload section
					$("#downloadProject").click(this.downloadProject);
					//add
					$("#add").click(temp.treatDom.add);
					//remove
					$("#remove").click(temp.treatDom.remove);
					//copy
					$("#copyPart").click(temp.treatDom.copy);
					//cut
					$("#cutPart").click(temp.treatDom.cut);
					//paste
					$("#pastePart").click(temp.treatDom.paste);

				},
				init: function () {
					this.ifEventFunc();
				}
			}
		}
		(),
		ifDownloadProject: function () {
			var download = function (filename, text) {
				var element = document.createElement('a');
				element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
				element.setAttribute('download', filename);

				element.style.display = 'none';
				document.body.appendChild(element);

				element.click();

				document.body.removeChild(element);
			};
			return {
				$section: $("#ifdownloadProject"),
				downloadButton: function () {
					//download ghostDom inner
					download(
						($('#projectName').val().includes('.html')) ? $('#projectName').val() : $('#projectName').val() + '.html',
						($('#ghostDom').contents().find('html').html().includes('<!doctype html>')) ? $('#ghostDom').contents().find('html').html() : '<!doctype html>' + $('#ghostDom').contents().find('html').html());
				},
				backToWorkTable: function () {
					temp.ui.interfaceChanger(temp.ui.ifWorkTable.$section);

				},
				ifEventFunc: function () {
					$("#backToWorkTable").click(this.backToWorkTable);
					$("#downloadProjectButton").click(this.downloadButton);
				},
				init: function () {
					this.ifEventFunc();
				}
			}
		}
		(),
		ifInit: function () {
			this.ifHome.init();
			this.ifCreate.init();
			this.ifRecent.init();
			this.ifWorkTable.init();
			this.ifDownloadProject.init();
		},
		toolbarUi: function () {
			return {
				/*add child
				remove*/
			}
		}
		(),
	}
}
();
