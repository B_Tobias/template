//ui
temp.ui.interfaceChanger($('#ifHome'));
//back to menu icon event
$(".back-to-menu").click(function () {
	temp.ui.interfaceChanger(temp.ui.ifHome.$section);
});
temp.ui.ifInit();

//auto save
if (typeof(Storage) !== "undefined") {
	temp.autoSave.support=true;	//auto save ON
	if (typeof localStorage.getItem("maxNum")=='undefined')
		localStorage.setItem("maxNum", "0");
}else {
	alert('Sorry! No Web Storage support..');	//auto save OFF
	temp.autoSave.support=false;
}
temp.autoSave.init();
