temp.treatDom = function () {
	var idMaxNum = 0,
	saved = [],
	isCutted = false,
	$ghostDom = $('#ghostDom').contents().find('html'),
	getType = function ($el) { //for ghost
		var classArr = $el.attr('class').split(' '),
		z = 0;
		while (z < classArr.length && !classArr[z].includes('part-'))
			z++;

		return classArr[z].replace('part-', '');
	},
	setId = function ($el, idNum) {
		if ($el.is('details'))
			$el.attr('id', 'det' + idNum);
		else
			$el.attr('id', 'ghost' + idNum);
	},
	getGhostChilds = function ($el) {
		var children = [],
		goAlongPart = function (arr) {
			for (var i = 0; i < arr.length; i++) {
				if ($(arr[i]).hasClass('part')) { //Is it child?Push to result
					children.push(arr[i]);
				}
				if ($(arr[i]).hasClass('child-container') && !$(arr[i]).hasClass('part')) { //Is it container? Call recursion for childrend
					goAlongPart($(arr[i]).children());
				}
			}
		};

		goAlongPart($el.children());
		return children;
	},
	getDetChildren = function ($det) {
		return $det.find('detail').push($det);
	},
	getGhostContainers = function ($el) {
		var containers = [],
		children = (typeof $el.children() == 'object') ? [$el] : $el.children();

		for (var i = 0; i < children.length; i++) {
			if (children[i].hasClass('child-container')) {
				containers.push(children[i]);
			}
		}
		return containers;
	},
	containerSetter = function ($det, $ghost) {
		var containers = [],
		$par = $ghost.closest(".part"),
		$selectContainer = $det.children('.detail-set-part').find('.detail-set-container'),
		children = (typeof $par.children() == 'object') ? [$par] : $par.children();
		//get containers
		for (var i = 0; i < children.length; i++) {
			if (children[i].hasClass('child-container')) {
				containers.push(children[i]);
			}
		}
		//stock up container selection
		for (var i in containers)
			$selectContainer.append('<option value=' + i + '>' + containers[i][0].tagName + containers[i].attr('class').replace(' ', '.') + '</option>');

		//set default value for container selection
		$selectContainer.val($par[0].tagName + $par.attr('class').replace(' ', '.'));
		//set change events
		$selectContainer.change(function () {
			containers[$(this).val()].append($ghost);
		});
	},
	createDet = function ($det) {
		//$container is from the detail tree
		return $(temp.html.partDetail).appendTo(($det.is('#domTree')) ? $det : $det.children('.detail-children')).css('margin-left', '1rem');
	},
	applyHandlerForDetails = function (hand) {
		var maxNum = idMaxNum;
		for (var i = 1; i <= maxNum; i++) //by using idMaxNum, this circle can go along every DETAIL(, or ghost) element
			if ($('#det' + i).find('.select-detail').first().prop('checked')) {
				$('#det' + i).find('.select-detail').first()[0].checked = false;
				hand($('#det' + i), $ghostDom.find('#ghost' + i));
			}
	};

	return {
		loadGhostDom: function (inner) {
			$ghostDom[0].innerHTML = inner;
		},
		loadToWorkTable: function () {
			var goAlongDom = function ($part, $ghostPar, $detPar) { //part is ghost el
				var children = [],
				$det = {},
				type = '';

				$part = ($part instanceof jQuery) ? $part : $($part);
				if (!$part.is('body')) {
					//set id for ghost
					//setId($part,idMaxNum);
					//create det
					$det = createDet($detPar);
					//increase id number becaus of new part
					idMaxNum++;
					//set id for det
					setId($det, idMaxNum);
					//set id for ghost(part)
					setId($part, idMaxNum);
					//get type
					type = getType($part);
					//set detail part class
					$det.addClass('part-' + type);
					//set detail part type
					$det.find('.part-type').first().text(type);
					//set allprops
					temp.part[type].setAllProperty($det, $part);
					//container setter
					containerSetter($det, $part);
					//set interface
					temp.ui.ifWorkTable.setDetailIf($det, 'children');
					//children for next round
					children = getGhostChilds($part);
					//detail interface changer:
					temp.ui.ifWorkTable.detailIfChanger($det);
					//set detail parent:
					$detPar = $det;

				} else {
					//children for next round
					children = $part.children('.part');
					//set detail parent:
					$detPar = $('#domTree');
				}
				//parents for next round
				$ghostPar = $part;
				//go along children
				for (var i = 0; i < children.length; i++) {
					goAlongDom(children[i], $ghostPar, $detPar);
				}
			};
			//start recursion with gostDom root
			goAlongDom($ghostDom.find('body'));
		},
		add: function () {
			applyHandlerForDetails(function ($det, $ghost) {
				var $newDet = {},
				$newGhost = {},
				partType = '',
				partContainer = '',
				$typeSetterSection = {},
				$containerSetterSection = {},
				containers = [],
				$par = $ghost.closest(".part"),
				$selectContainer = {},
				typeOptions = function () { //get type option html inners
					var optionsHtml = '';
					for (var key in temp.part) {
						if (temp.part.hasOwnProperty(key)) {
							optionsHtml += '<option value="' + key + '">' + key + '</option>';
						}
					}
					return optionsHtml;
				};
				//increase id number becaus of new part
				idMaxNum++;
				//create detail
				$newDet = createDet($det);
				//detail interface changer:
				temp.ui.ifWorkTable.detailIfChanger($newDet);
				//set id for detail
				setId($newDet, idMaxNum);
				//add type setter
				$typeSetterSection = $(temp.html.detailSetType).appendTo($newDet);
				//set detail interface
				temp.ui.ifWorkTable.setDetailIf($newDet, 'type-first-time');
				//stock up select
				for (var key in temp.part) {
					if (temp.part.hasOwnProperty(key)) {
						$typeSetterSection.find('.detail-set-type-first-time').append('<option value="' + key + '">' + key + '</option>');
					}
				}
				//set type
				$typeSetterSection.find('.detail-set-type-first-time-button').click(function () {
					//save selected part type
					partType = $typeSetterSection.find('select').val();
					//print detailtype name
					$newDet.find('.part-type').first().text(partType);
					//remove type setter
					$(this).parent().remove();

					//container setter
					$containerSetterSection = $(temp.html.detailSetContainer).appendTo($newDet);
					//set detail interface
					temp.ui.ifWorkTable.setDetailIf($newDet, 'container-first-time');
					//get containers
					containers = getGhostContainers($par);
					console.log(containers);
					//get selection
					$selectContainer = $containerSetterSection.find('.detail-set-container-first-time');
					//stock up container selection
					for (var i in containers)
						$selectContainer.append('<option value=' + i + '>' + containers[i][0].tagName + containers[i].attr('class').replace(' ', '.') + '</option>');

					//set default value for container selection
					$selectContainer.val($par[0].tagName + $par.attr('class').replace(' ', '.'));
					//set container
					$containerSetterSection.find('.detail-set-container-first-time-button').click(function () {
						//save container
						partContainer = $('.detail-set-container-first-time>option:selected').attr('value');
						console.log(partContainer);
						//create ghost
						$newGhost = $(temp.part[partType].inner).appendTo(containers[partContainer]);
						//set id for ghost
						setId($newGhost, idMaxNum);
						//set allprops
						temp.part[partType].setAllProperty($newDet, $newGhost);
						//container setter
						containerSetter($newDet, $newGhost);
						//set interface
						temp.ui.ifWorkTable.setDetailIf($newDet, 'children');
						//remove container setter
						$(this).parent().remove();
					});
				});
			});
		},
		remove: function () {
			applyHandlerForDetails(function ($det, $ghost) {
				$det.remove();
				$ghost.remove();
			});
		},
		copy: function () {
			applyHandlerForDetails(function ($det, $ghost) {
				saved.push($det.attr('id').replace('det', ''));
			});
		},
		cut: function () {
			applyHandlerForDetails(function ($det, $ghost) {
				saved.push($det.attr('id').replace('det', ''));
				isCutted = true;
				$det.hide();
			});
		},
		paste: function () {
			applyHandlerForDetails(function ($det, $ghost) {
				var containers = getGhostContainers($ghost),
				$containerSetterSection = {},
				$selectContainer = {},
				$thatGhost = {},
				$thatDet = {},
				detChildren = [],
				ghostChildren = [],
				z = 0;
				//container setter
				$containerSetterSection = $(temp.html.detailSetContainer).appendTo($det);
				//set detail interface
				temp.ui.ifWorkTable.setDetailIf($det, 'container-first-time');
				//get selection
				$selectContainer = $containerSetterSection.find('.detail-set-container-first-time');
				//stock up container selection
				for (var i in containers)
					$selectContainer.append('<option value=' + i + '>' + containers[i][0].tagName + containers[i].attr('class').replace(' ', '.') + '</option>');

				$containerSetterSection.find('.detail-set-container-first-time-button').click(function () {
					//paste data
					if (isCutted) {
						//cutted
						isCutted = false;
						for (var i in saved) {
							$thatGhost = $ghostDom.find('#ghost' + saved[i]);
							$thatDet = $('#det' + saved[i]);
							//paste details,ghosts
							$det.find('.detail-children').first().append($thatDet);
							containers[$('.detail-set-container-first-time>option:selected').attr('value')].append($thatGhost);
							//set cont hand
							containerSetter($thatDet, $thatGhost);
							//show detail
							$thatDet.show();
						}
					} else {
						//copied
						//clear childs from new array
						for (var i in saved) {
							//checked element
							$copiedTarget = $('#det' + saved[i]);
							//check for every saved
							while ($copiedTarget.parents(saved[z]).length && z < saved.length)
								z++;
							if (z < saved.length - 1)
								delete saved[z]; //remove element
						}
						//get id number array
						for (var i in saved) {
							$thatDet = $('#det' + saved[i]).clone();
							$thatGhost = $ghostDom.find('#ghost' + saved[i]).clone();

							//paste details,ghosts
							$det.find('.detail-children').first().append($thatDet);
							containers[$('.detail-set-container-first-time>option:selected').attr('value')].append($thatGhost);

							//go along children and set things
							//get children
							detChildren = $thatDet.find('details').toArray(); //.push($thatDet.get(0));//push main element
							detChildren.unshift($thatDet[0]);
							//get children
							ghostChildren = $thatGhost.find('.part').toArray(); //push main element
							ghostChildren.unshift($thatGhost[0]);

							for (var j in detChildren) {
								if (typeof detChildren[j] != 'number' && !detChildren[j].length) {
									//dom obj to jquery
									detChildren[j] = $(detChildren[j]);
									ghostChildren[j] = $(ghostChildren[j]);

									//set new ids
									idMaxNum++;
									setId(ghostChildren[j], idMaxNum);
									setId(detChildren[j], idMaxNum);

									//clear detail interface:
									detChildren[j].children('.detail-content').empty();
									detChildren[j].find('.detail-set-part>select').empty();

									//set cont hand
									containerSetter(detChildren[j], ghostChildren[j]);

									//set allprops
									temp.part[getType(ghostChildren[j])].setAllProperty(detChildren[j], ghostChildren[j]);

									//detail interface changer:
									temp.ui.ifWorkTable.detailIfChanger(detChildren[j]);

									//show detail
									detChildren[j].show();
								}
							}
						}

					}
					//set interface
					temp.ui.ifWorkTable.setDetailIf($det, 'children');
					//remove container setter
					$(this).parent().remove();
					//clear memory
					saved = [];
					allCopiedArr = [];
				});

			});
		},

	}
}

();
