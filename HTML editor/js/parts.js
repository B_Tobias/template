//classes
temp.part = function () {
	var partClass = function (name, inner, propertis) {
		return {
			name: name,
			inner: inner,
			propertis: propertis,
			setAllProperty: function ($det, $ghost) {
				var setters = this.propertis.map(function(prop){
					return prop.setter;
				});
				for (var i in setters)
					setters[i]($det, $ghost);
			}
		}
	},
	propertyClass = function (propertyName, possibleValue, Defaultvalue, setter) {
		return {
			propertyName: propertyName,
			possibbleValue: possibleValue,
			Defaultvalue: Defaultvalue,
			setter: setter
		}
	};
	return {
		container: partClass(
			'container',
			`<article class="part part-container child-container"></article>`, 
			[propertyClass('align', ['left', 'center', 'right'], 'center', function ($det, $ghost) {
					//append input
					var $inp=$(temp.html.propertyOptional.replace('PROPNAME','align')).appendTo($det.children('.detail-content')),
					posProps=['left', 'center', 'right'];
					//stock up input
					for (var i in posProps)
						$inp.find('select').append('<option value="'+posProps[i]+'">'+posProps[i]+'</option>')
					
					//set event
					$inp.find('select').change(function () {
						$ghost.css('text-align', $inp.find('select').val());
					});
				})
			])
	}
}
();
/*
//part obj container
temp.partJS = {};
//classes
temp.part = function () {
var getPartChilds = function ($el) {
var children = [],
goAlongPart = function (arr) {
for (var i = 0; i < arr.length; i++) {
if ($(arr[i]).hasClass('part')) {
children.push(arr[i]);
}
if ($(arr[i]).hasClass('child-container') && !$(arr[i]).hasClass('part')) {
goAlongPart($(arr[i]).children());
}
}
};

goAlongPart($el.children());
return children;
},
getPartContainers = function ($el) {
var conts = [],
arr = (typeof $el.children() == 'object') ? [$el] : $el.children();
for (var i = 0; i < arr.length; i++) {
if (arr[i].hasClass('child-container')) {
conts.push(arr[i]);
}
}
return conts;
},
propertyClass = function (propName, posVal, val, method) {
return {
propertyName: propName,
posibleVal: posVal,
value: val,
setMethod: method
};
},
handlerProp = function (that) {
var $detail = that.$elDetail,
$propEl = {},
props = that.propertis,
prop = {},
$cont = $detail.find('.detail-content');
for (var i = 0; i < props.length; i++) {
if (Object.prototype.toString.call(props[i].posibleVal) == '[object Array]') {
//optional
//append property
$propEl = $(temp.html.propertyOptional.replace('PROPNAME', props[i].propertyName)).appendTo($cont);
//append optional props
for (var j = 0; j < props[i].posibleVal.length; j++)
$propEl.find('.detail-prop-optional').last().append('<option>' + props[i].posibleVal[j] + '</option>');
//change event for optional props
prop = props[i];
$propEl.find('.detail-prop-optional').change(function () {
prop.value = $(this).find('option:selected').text();
prop.setMethod(that);
});
} else {
//string
//append property
$propEl = $(temp.html.propertyOptional.replace('PROPNAME', props[i].propertyName)).appendTo($cont);
//blur event for string props--->setMethod
prop = props[0];
$propEl.find('.detail-prop-string').change(function () {
prop.value = $(this).find('option:selected').text();
prop.setMethod(that);
});
}
}
};
return {
container: function ($elDet, $elGhost, idNumber) {
return {
partType: "container",
inner: function () {
return `<div class="part part-` + this.partType + ` child-container"></div>`
},
idNum: idNumber,
$elDetail: $elDet,
$elGhost: $elGhost,
childs: function () {
return getPartChilds($elGhost);
},
parent: function () {
return $elGhost.closest(".part");
},
containers: function () {
return getPartContainers(this.$elGhost);
},
propertis: [propertyClass('align', ['left', 'center', 'right'], 'center', function (that) {
console.log(that);
var arr = that.$elGhost.attr('class').split(' '),
myProp = '';
for (var i = 0; i < arr.length; i++)
if (arr[i].includes('part-prop-'))
myProp = arr[i];

that.$elGhost.removeClass(myProp);
that.$elGhost.addClass('part-prop-' + this.value);

})],
handlers: function () {
handlerProp(this);
},
}
//.part-container{display:block;width:100%;}
}
}
}
();
*/
