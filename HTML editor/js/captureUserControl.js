/*
Capture user control:
imitate user control for faster development

Usage:
type to console:
cuc.on();//--->now cuc capture every mouseDown,keyDown,doubleClick event
cuc.off();//--->stop capturing events. Result will be user controll

 */

//include jquery if needed
if (typeof jQuery == 'undefined') {
	document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>');
}

window.cuc = window.cuc || {};
cuc = function () {
	var resultArr = [], //arr of obj {val,type}
	getPathInDom = function ($el) {
		var path = ''
			parentsOfEl = $el.parents().toArray();
		//wich element
		path = $el[0].nodeName.toLowerCase() + ':nth-child(' + ($el.index() + 1) + ')';
		//parents
		for (var j in parentsOfEl) {
			path = parentsOfEl[j].nodeName.toLowerCase() + ':nth-child(' + ($(parentsOfEl[j]).index() + 1) + ')>' + path;
		}
		return path;
	};
	return {
		on: function () {
			var clicks = 0,
			delay = 400;
			//start capturing(bind capture method for:)
			$(document).on('mousedown.cuc', function (event) {
				//clicked el--->event.target
				event.preventDefault();
				clicks++;

				setTimeout(function () {
					clicks = 0;
				}, delay);

				if (clicks === 2) {
					resultArr.push({
						val: $(event.target),
						type: 'dblclick',
						path: getPathInDom($(event.target))
					});
					clicks = 0;
					return;
				} else {
					resultArr.push({
						val: $(event.target),
						type: 'click',
						path: getPathInDom($(event.target))
					});
				}
			});

			$(document).on('keydown.cuc', function (event) {
				//pressed keyboard key--->event.keyCode
				resultArr.push({
					val: event.which,
					type: 'keydown'
				});
			});

			return 'capturing started'
		},
		off: function () {
			var codeLine = '',
			parentsOfEl = [];
			//remove events/stop capturing
			$(document).unbind('mousedown.cuc');
			$(document).unbind('keydown.cuc');
			//save result to cuc.result
			codeLine = 'var evCUC=jQuery.Event("keydown");\n'; //imitation keypress
			for (var i in resultArr) {
				switch (resultArr[i].type) {
				case 'click':
					codeLine = '$("' + resultArr[i].path + '").click()';
					break;
				case 'dblclick':
					codeLine = '$("' + resultArr[i].path + '").dblclick()';
					break;
				case 'keydown':
					codeLine = 'evCUC.which = ' + resultArr[i].val + ';$(document).trigger(evCUC);';
					break;
				}

				cuc.result += codeLine + ';\n';
				codeLine = '';
			}

			//result
			return cuc.result;
		},
		result: ''
	}
}
();
