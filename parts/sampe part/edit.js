//this will be a class
temp.part.sample = function () {
	return {
		inner: `<article class="part part-name"><div class="child-container"></div></article>`,
		$el: $el,
		propertyString: {
			propertyName: 'propertyString',
			posVal: 'string',
			value: '',
			setMethod: function ($myPart) {
				console.log('set property for part');
			}
		},
		propertyOptional: {
			propertyName: 'propertyOptional',
			posVal: ['option1', 'option2'],
			value: '',
			setMethod: function ($myPart) {
				console.log('set property for part');
			}
		}
	}
};