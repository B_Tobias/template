//this will be a class
temp.part.sample = function ($el) {
	return {
		inner: `<div class="part part-container child-container"></div>`,
		$el:$el,
		floating:{
			propertyName:'floating',
			valueType:['left','center','right'],
			value:'center',
			setMethod:function($myPart){
				$myPart.css('float',this.value);
			}
		}
	}
};